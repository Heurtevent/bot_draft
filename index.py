import os
from typing import Text
import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv(dotenv_path="config")

bot = commands.Bot(command_prefix = "!", description = "Bot de RYPERK")

@bot.event
async def on_ready():
	print("Ready !")

@bot.command(name="del")
async def delete(ctx, number_of_messages: int):
    messages = await ctx.channel.history(limit=number_of_messages +1).flatten()

    for each_message in messages:
        await each_message.delete()

@bot.command()
async def pick(ctx):
	await ctx.send("Envoyez les sorts que vous voulez pick")

	def checkMessage(message):
		return message.author == ctx.message.author and ctx.message.channel == message.channel

	def checkEmoji(reaction, user):
		return ctx.message.author == user and message.id == reaction.message.id and (str(reaction.emoji) == "✅" or str(reaction.emoji) == "❌")
	try:
		x = 0
		list = []
		while (x < 5):
			x += 1
			sort = await bot.wait_for("message", timeout = 60, check = checkMessage)
			message = await ctx.send(f"Le sort {sort.content} a été pick. Veuillez valider en réagissant avec ✅. Sinon réagissez avec ❌")
			list.append(sort.content)
			await message.add_reaction("✅")
			await message.add_reaction("❌")
			reaction, user = await bot.wait_for("reaction_add", timeout = 60, check = checkEmoji)
			if reaction.emoji == "✅":
				await ctx.send("Veuillez pick un nouveau sort.")
			else:
				await ctx.send("Le pick a bien été annulé.")
	except:
		await ctx.send("Veuillez réitérer le sort.")

	list=' '.join([str(item)for item in list])
	print(list)
	await ctx.send("Vous avez pick **{}** Veuillez à présent ban les sorts avec !baan.".format(list))



@bot.command()
async def baan(ctx):
	await ctx.send("Envoyez les sorts que vous voulez ban")

	def checkMessage(message):
		return message.author == ctx.message.author and ctx.message.channel == message.channel

	def checkEmoji(reaction, user):
		return ctx.message.author == user and message.id == reaction.message.id and (str(reaction.emoji) == "✅" or str(reaction.emoji) == "❌")
	try:
		x = 0
		list = []
		while (x < 5):
			x += 1
			sort = await bot.wait_for("message", timeout = 60, check = checkMessage)
			message = await ctx.send(f"Le sort {sort.content} a été ban. Veuillez valider en réagissant avec ✅. Sinon réagissez avec ❌")
			list.append(sort.content)
			await message.add_reaction("✅")
			await message.add_reaction("❌")
			reaction, user = await bot.wait_for("reaction_add", timeout = 60, check = checkEmoji)
			if reaction.emoji == "✅":
				await ctx.send("Veuillez ban un nouveau sort.")
			else:
				await ctx.send("Le ban a bien été annulé.")
	except:
		await ctx.send("Veuillez réitérer le sort.")

	list=' '.join([str(item)for item in list])
	print(list)
	await ctx.send("Vous avez ban **{}**.".format(list))
	await ctx.send("La draft est terminée. Bon match!")
	

bot.run(os.getenv("TOKEN"))

"""

"""
"""
✅❌
"""

